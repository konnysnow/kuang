package com.kuang.mapper;

import com.kuang.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    public List<User> selectUser();
    public int insertUser(User user);
    public int deleteUser( int id);
//    public void deleteUser(@Param int id);
}
