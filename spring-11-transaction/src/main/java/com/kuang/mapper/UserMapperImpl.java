package com.kuang.mapper;

import com.kuang.pojo.User;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import java.util.List;

public class UserMapperImpl extends SqlSessionDaoSupport implements UserMapper{


    @Override
    public List<User> selectUser() {
        User user = new User();
        user.setId(7);
        user.setName("张三7");
        user.setPwd("343214421234");
        insertUser(user);

        deleteUser(4);

        return getSqlSessionTemplate().getMapper(UserMapper.class).selectUser();
    }

    @Override
    public int insertUser(User user) {
        return getSqlSessionTemplate().getMapper(UserMapper.class).insertUser(user);
    }

    @Override
    public int deleteUser(int id) {
        return getSqlSessionTemplate().getMapper(UserMapper.class).deleteUser(id);
    }
}
