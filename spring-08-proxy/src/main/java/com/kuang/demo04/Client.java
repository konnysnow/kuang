package com.kuang.demo04;

import com.kuang.demo02.UserService;
import com.kuang.demo02.UserServiceImpl;
import com.kuang.demo02.UserServiceOracleImpl;

public class Client {
    public static void main(String[] args) {
        //真实角色，被代理角色
        UserServiceImpl userServiceImpl = new UserServiceImpl();

        ProxyInvocationHandler pih =new ProxyInvocationHandler();
        pih.setTarget(userServiceImpl);
        //动态创建代理类（的对象）
        UserService proxy = (UserService) pih.getProxy();
        proxy.query();
        proxy.add();

//更换一个被代理类（真实角色）！！
        UserServiceOracleImpl userService2 = new UserServiceOracleImpl();
        pih.setTarget(userService2);
        proxy.add();
    }
}
