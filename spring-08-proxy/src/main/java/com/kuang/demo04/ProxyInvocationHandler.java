package com.kuang.demo04;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 通用的代理类handler，也可以生成代理类！！！
 * 1。实现InvocationHandler，有invoke功能
 * 2。getProxy()获取代理，
 * 这样二者合一了，就不用写的太复杂了。。。。
 */
public class ProxyInvocationHandler implements InvocationHandler {
    //被代理的对象
    private Object target;
    //注入
    public void setTarget(Object target) {
        this.target = target;
    }
    //生成得到代理类
    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader()
                , target.getClass().getInterfaces()//FIXME 注意，这里是接口
                ,this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log(method.getName());
        Object invoke = method.invoke(target, args);
        return invoke;
    }

    public void log(String methodName){
        System.out.println("调用了"+methodName+"方法");
    }
}
