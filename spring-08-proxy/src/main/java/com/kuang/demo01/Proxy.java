package com.kuang.demo01;

public class Proxy implements RentHouse{
    private Host host;
    private int count;

    public Proxy() {
    }

    public Proxy(Host host,int count) {
        this.host = host;
        this.count = count;
    }

    @Override
    public void rent() {
        for (int i=0;i<count;i++) {
            this.seeHouse(i+1);
        }

        host.rent();
        this.dealDone();
    }

    public void seeHouse(int i){
        System.out.println("中介带你看第"+i+"套房子");
    }
    public void dealDone(){
        System.out.println("看中房子，签合同付钱");
    }
}
