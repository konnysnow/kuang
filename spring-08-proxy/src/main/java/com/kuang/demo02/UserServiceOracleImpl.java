package com.kuang.demo02;

public class UserServiceOracleImpl implements UserService{
    @Override
    public void add() {
        System.out.println("【　我是oracle】增加一个user");
    }

    @Override
    public void delete() {

        System.out.println("【　我是oracle】删除一个user");
    }

    @Override
    public void update() {

        System.out.println("【　我是oracle】修改一个user");
    }

    @Override
    public void query() {

        System.out.println("【　我是oracle】查询一个user");
    }
}
