package com.kuang.demo02;

public class Client {

    public static void main(String[] args) {

        UserService userService = new UserServiceImpl();
        //userService.add();

        UserProxy userProxy = new UserProxy();
        userProxy.setUserService(userService);

        userProxy.add();
        userProxy.delete();
        userProxy.update();
        userProxy.query();
    }
}
