package com.kuang.demo02;

public class UserServiceImpl implements UserService{
    @Override
    public void add() {
        System.out.println("增加一个user");
    }

    @Override
    public void delete() {

        System.out.println("删除一个user");
    }

    @Override
    public void update() {

        System.out.println("修改一个user");
    }

    @Override
    public void query() {

        System.out.println("查询一个user");
    }
}
