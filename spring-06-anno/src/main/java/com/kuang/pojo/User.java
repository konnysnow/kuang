package com.kuang.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// 相当于这个   <bean id="user" class="com.kuang.pojo.User"></bean>
//组件
@Component
@Scope("singleton")//@Scope("prototype")
public class User {
    @Value("张三")
    public String name="搞卫生2";
    @Value("张三2")
    public void setName(String name) {
        this.name = name;
    }
}
