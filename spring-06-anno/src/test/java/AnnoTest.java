import com.kuang.pojo.Cat;
import com.kuang.pojo.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnoTest {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        User user=context.getBean("user",User.class);
        //测试@Scope("protptype")，xml-bean和注解@Component都存在user，默认xml优先
        System.out.println(user.name);

        //测试@Scope("protptype")
        Cat cat = context.getBean("cat", Cat.class);
        Cat cat2 = context.getBean("cat", Cat.class);
        System.out.println(cat==cat2);
        System.out.println(cat.hashCode());
        System.out.println(cat2.hashCode());
    }


}
