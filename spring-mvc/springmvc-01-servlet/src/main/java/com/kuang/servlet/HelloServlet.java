package com.kuang.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String m=req.getParameter("method");
        if("add".equals(m)){
            req.setAttribute("msg2",m);
            req.getSession().setAttribute("msg","执行了"+m+"方法");
            req.getRequestDispatcher("/WEB-INF/jsp/test.jsp").forward(req,resp);
        }else if("delete".equals(m)){
            req.setAttribute("msg2",m);
            req.getSession().setAttribute("msg","执行了"+m+"方法,客户端跳转，取不到req.setAttribute里的内容了");
//            resp.sendRedirect("WEB-INF/jsp/form222.jsp");//相对路径，从web目录开始。web-inf目录下的jsp都是受保护的，不能直接访问！
            resp.sendRedirect("form.jsp");//相对路径
        }else if("baidu".equals(m)){
            req.setAttribute("msg2",m);
            req.getSession().setAttribute("msg","执行了"+m+"方法");
            resp.sendRedirect("http://www.baidu.com");
        }else{
            req.getRequestDispatcher("/WEB-INF/jsp/test.jsp").forward(req,resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
