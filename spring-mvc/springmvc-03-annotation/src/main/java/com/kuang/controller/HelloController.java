package com.kuang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping("/test")
    public String hello(Model model){
        model.addAttribute("msg",1111);
        return "test";
    }
    @RequestMapping("/aaa")
    public String aaa(Model model){
        model.addAttribute("msg","aaa");
        return "aaa";
    }
}
