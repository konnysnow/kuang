import com.kuang.mapper.NoteMapper;
import com.kuang.pojo.Note;
import com.kuang.utils.MybatisUtils;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestImportCnote {


/*
[2017/7/11 11:19] Android Studio 配置

软件
C:\Program Files\Android\Android Studio

sdk目录
C:\Users\IBM\AppData\Local\Android\sdk

Tags:
Android Studio 配置



[2017/6/7 17:15] dbm和w的转换

10w=10000mw=40dbm

40dbm=10*log(10,10*1000).

Tags:
cdma dbm w 瓦



 */

    @Test
    public void insertCnote() throws IOException {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);


        List<Note> list = new ArrayList<Note>();

        String fileName="/Users/konnysnow/Documents/dell/back/back2/2021-07-12--gws--cintanotes (exported 2021-07-12_1345).txt";
        File file = new File(fileName);
        List<String> lines = FileUtils.readLines(file, "utf-16le");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/d hh:mm");

        Note theNote = null;
        String theContent = null;
        String theOri = "";
        boolean isTag=false;//
        boolean isNewLine = false;
        int i=0;
        for (String line : lines) {
            i++;
            //System.out.println(i);
            //System.out.println(line);

            int startIndex = line.indexOf("[");
            int endIndex = line.indexOf("]");
            String dateStr =null;
            Date createDate=null;
            if(endIndex>startIndex) {
                System.out.println(i);
                dateStr = line.substring(startIndex + 1, endIndex);
                try {
                    createDate = sdf.parse(dateStr);
                    isNewLine = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    theNote.setRemark("sdf-err=" + e.getMessage());
                    isNewLine = false;
                }
            }
            if(isNewLine){
                if(theNote!=null) {
                    theNote.setOriStr(theOri);
                    theNote.setContent(theContent);
                    Note notex = new Note();
                    BeanUtils.copyProperties(theNote,notex);
                    list.add(notex);
                }
                theNote = new Note();
                theNote.setCreateTimeStr(dateStr);
                theNote.setCreateTime(createDate);
                theContent = new String();
                theOri = new String();
                theOri+=line+"\n";
                isTag=false;//
                isNewLine=false;

                String title = line.substring(endIndex+1);
                theNote.setTitle(title);
            }else {
                theOri+=line+"\n";
                if (isTag) {
                    if (line.indexOf("(") == 0) {//link那一行
                        theNote.setLink(line.replace("(", "").replace(")", ""));
                    } else if(!"".equals(line)){//tag那一行
                        theNote.setTag(line);
                    }
                }
                if (line.startsWith("Tags:"))
                    isTag = true;
                if (isTag == false)
                    theContent += line + "\n";
            }
        }

        //最后一个也加入进去。
        theNote.setOriStr(theOri);
        list.add(theNote);


        for (Note note : list) {
            int x =mapper.insertNote(note);
            //System.out.println(x);
        }
        sqlSession.commit();
        sqlSession.close();
    }

    //@Test
    public void selectCnote(){
        SqlSession sqlSession =null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
            List<Note> notes = mapper.selectNote();
//            for (Note note : notes) {
//                System.out.println(note.toString());
//            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(sqlSession!=null)
                sqlSession.close();
        }
    }
}
