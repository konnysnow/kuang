import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import com.kuang.pojo.Note;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestVoice {
    /**
     * @Title: strat
     * @Description: 该方法的主要作用：朗读
     * @param  @param content
     * @param  @param type 设定文件   0:开始，1停止
     * @return  返回类型：void
     * @throws
     */
    public void strat(String content, int type) {
        ActiveXComponent sap = new ActiveXComponent("Sapi.SpVoice");
        Dispatch sapo = sap.getObject();
        if (type == 0) {
            try {
                // 音量 0-100
                sap.setProperty("Volume", new Variant(100));
                // 语音朗读速度 -10 到 +10
                sap.setProperty("Rate", new Variant(1.3));
                Variant defalutVoice = sap.getProperty("Voice");
                Dispatch dispdefaultVoice = defalutVoice.toDispatch();
                Variant allVoices = Dispatch.call(sapo, "GetVoices");
                Dispatch dispVoices = allVoices.toDispatch();
                Dispatch setvoice = Dispatch.call(dispVoices, "Item",
                        new Variant(1)).toDispatch();
                ActiveXComponent voiceActivex = new ActiveXComponent( dispdefaultVoice);
                ActiveXComponent setvoiceActivex = new ActiveXComponent(setvoice);
                Variant item = Dispatch.call(setvoiceActivex, "GetDescription");
                // 执行朗读
                Dispatch.call(sapo, "Speak", new Variant(content));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sapo.safeRelease();
                sap.safeRelease();
            }
        } else {
            // 停止
            try {
                Dispatch.call(sapo, "Speak", new Variant(content), new Variant(2));
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
    /**
     *
     * @Title: test
     * @Description: 该方法的主要作用：执行朗读内容
     * @param   设定文件
     * @return  返回类型：void
     * @throws
     */
    @Test
    public void test()
    {
        strat("110千伏金华站", 0);
    }


    public static void main(String[] args) throws ParseException {
        String line="[2011/09/05 10:15] dblink";

        int startIndex = line.indexOf("[");
        int endIndex = line.indexOf("]");
        if(startIndex==0 && endIndex>0 && startIndex<endIndex){

            String dateStr = line.substring(startIndex+1,endIndex);
            String title = line.substring(endIndex+2);
            System.out.println(title);
            System.out.println(dateStr);


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/M/d HH:mm");
            System.out.println(sdf.parse(dateStr));
        }
    }
}
