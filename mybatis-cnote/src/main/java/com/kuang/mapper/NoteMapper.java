package com.kuang.mapper;

import com.kuang.pojo.Note;

import java.util.List;

public interface NoteMapper {
    List<Note> selectNote();
    List<Note> selectNoteByPage();
    int insertNote(Note note);

    public int updateNote(Note note);
    public Note selectNoteById(Long id);
}
