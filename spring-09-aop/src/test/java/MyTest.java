import com.kuang.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        //动态代理类，代理的是接口，这里必须写接口：UserService.class
        //jdk的动态代理，是基于接口的！！！！
        UserService userService = ctx.getBean("userService", UserService.class);
        userService.add();
        //userService.select();
    }
}
