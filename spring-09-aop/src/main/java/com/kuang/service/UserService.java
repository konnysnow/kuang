package com.kuang.service;

public interface UserService {
    public String add();
    public Integer delete();
    public void update();
    public void select();
}
