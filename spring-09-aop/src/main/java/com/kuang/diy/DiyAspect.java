package com.kuang.diy;

/**自定义切面类*/
public class DiyAspect {
    public void before(){
        System.out.println("========执行前=====");
    }
    public void after(){
        System.out.println("========执行后=====");
    }
}
