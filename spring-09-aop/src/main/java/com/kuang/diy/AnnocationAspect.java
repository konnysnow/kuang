package com.kuang.diy;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;

@Aspect
public class AnnocationAspect {
    @Before("execution(* com.kuang.service.UserServiceImpl.*(..))")
    public void before(){
        System.out.println("=======执行前=11==");
    }
    @After("execution(* com.kuang.service.UserServiceImpl.*(..))")
    public void after(){
        System.out.println("=======执行后22===");
    }
    @AfterReturning(returning="rvt", value="execution(* com.kuang.service.UserServiceImpl.*(..)) && args()")
    public  void AfterReturning(Object rvt){
        System.out.println("=======执行后33333333===返回值=【"+rvt+"】");
    }
//      @AfterReturning(returning="rvt", pointcut="@annotation(com.kuang.service.UserServiceImpl)")
//    public  Object AfterReturning(JoinPoint joinPoint,Object rvt){
//        System.out.println("=======执行后33===返回值=【"+rvt+"】");
//        return rvt;
//    }
    @Around("execution(* com.kuang.service.UserServiceImpl.*(..))")
    public Object around(ProceedingJoinPoint jp) throws Throwable {
        System.out.println("-------环绕前-------");
        Signature signature = jp.getSignature();//执行了哪个接口的，哪个方法！！
        System.out.println("-------signature=["+signature+"]-------");
        Object proceed = jp.proceed();
        System.out.println("-------执行结果，proceed=["+proceed+"]-------");
        System.out.println("-------环绕后-------");
        return proceed;//----------------------------------环绕，如果不把结果返回，AfterReturning，就获取不到结果！！！！！！
    }
    /**
     -------环绕前-------
     -------signature=[String com.kuang.service.UserService.add()]-------
     =======执行前=11==
     新增了一个用户
     -------执行结果，proceed=[ok111]-------
     -------环绕后-------
     =======执行后22===
     =======执行后33333333===返回值=【ok111】
     */
}
