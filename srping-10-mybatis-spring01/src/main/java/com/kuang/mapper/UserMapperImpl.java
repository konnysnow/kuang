package com.kuang.mapper;

import com.kuang.pojo.User;
import org.mybatis.spring.SqlSessionTemplate;

import java.util.List;

public class UserMapperImpl implements UserMapper{

    public SqlSessionTemplate sqlSessionTemplate;

    public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
        System.out.println("------UserMapperImpl，依赖注入setSqlSessionTemplate=["+sqlSessionTemplate+"]--------");
        this.sqlSessionTemplate = sqlSessionTemplate;
    }
    public String name;

    public void setName(String name) {
        System.out.println("------UserMapperImpl，依赖注入setName=["+name+"]--------");
        this.name = name;
    }

    public UserMapperImpl(){
    System.out.println("------UserMapperImpl，实例化--------");
}
public void testProcessInit(){
    System.out.println("------UserMapperImpl，初始化方法testProcessInit,测试后置处理器--------");
}
    @Override
    public List<User> selectUser() {
        return sqlSessionTemplate.getMapper(UserMapper.class).selectUser();
    }
}
