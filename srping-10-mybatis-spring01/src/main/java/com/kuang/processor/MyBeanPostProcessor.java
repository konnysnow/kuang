package com.kuang.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 注意:接口中两个方法不能返回null，如果返回null,那么在后续初始化方法将报空指针异常,或者通过getBean()方法获取不到bena实例对象，
 因为后置处理器从Spring IoC容器中取出bean实例对象没有再次放回IoC容器中
 */
public class MyBeanPostProcessor implements BeanPostProcessor {
    /**
     * 实例化、依赖注入完毕，在调用显示的初始化之前完成一些定制的初始化任务
     * 注意：方法返回值不能为null
     * 如果返回null那么在后续初始化方法将报空指针异常或者通过getBean()方法获取不到bena实例对象
     * 因为后置处理器从Spring IoC容器中取出bean实例对象没有再次放回IoC容器中
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("before--实例化的bean对象:"+bean+"\t"+beanName);
        // 可以根据beanName不同执行不同的处理操作
        return bean;
    }

    /**
     * 实例化、依赖注入、初始化完毕时执行
     * 注意：方法返回值不能为null
     * 如果返回null那么在后续初始化方法将报空指针异常或者通过getBean()方法获取不到bena实例对象
     * 因为后置处理器从Spring IoC容器中取出bean实例对象没有再次放回IoC容器中
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("after...实例化的bean对象:"+bean+"\t"+beanName);
        // 可以根据beanName不同执行不同的处理操作
        return bean;
    }
//    @Override
    public int getOrder() {
        // TODO Auto-generated method stub
        return 1;
    }
}
/*
before--实例化的bean对象:org.springframework.jdbc.datasource.DriverManagerDataSource@68ceda24	dataSource
after...实例化的bean对象:org.springframework.jdbc.datasource.DriverManagerDataSource@68ceda24	dataSource
before--实例化的bean对象:org.mybatis.spring.SqlSessionFactoryBean@166fa74d	sqlSessionFactory
Logging initialized using 'class org.apache.ibatis.logging.stdout.StdOutImpl' adapter.
Class not found: org.jboss.vfs.VFS
JBoss 6 VFS API is not available in this environment.
Class not found: org.jboss.vfs.VirtualFile
VFS implementation org.apache.ibatis.io.JBoss6VFS is not valid in this environment.
Using VFS adapter org.apache.ibatis.io.DefaultVFS
Find JAR URL: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo
Not a JAR: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo
Reader entry: User.class
Listing file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo
Find JAR URL: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo/User.class
Not a JAR: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo/User.class
Reader entry: ����   4 ]
Checking to see if class com.kuang.pojo.User matches criteria [is assignable to Object]
after...实例化的bean对象:org.mybatis.spring.SqlSessionFactoryBean@166fa74d	sqlSessionFactory
after...实例化的bean对象:org.apache.ibatis.session.defaults.DefaultSqlSessionFactory@6c64cb25	sqlSessionFactory
before--实例化的bean对象:org.mybatis.spring.SqlSessionTemplate@130d63be	sqlSessionTemplate
after...实例化的bean对象:org.mybatis.spring.SqlSessionTemplate@130d63be	sqlSessionTemplate
------UserMapperImpl，实例化--------
------UserMapperImpl，依赖注入setSqlSessionTemplate=[org.mybatis.spring.SqlSessionTemplate@130d63be]--------
------UserMapperImpl，依赖注入setName=[oscar]--------
before--实例化的bean对象:com.kuang.mapper.UserMapperImpl@42a48628	userMapper
------UserMapperImpl，初始化方法testProcessInit,测试后置处理器--------
after...实例化的bean对象:com.kuang.mapper.UserMapperImpl@42a48628	userMapper
 */