package com.kuang.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor2 implements BeanPostProcessor {
    /**
     * 实例化、依赖注入完毕，在调用显示的初始化之前完成一些定制的初始化任务
     * 注意：方法返回值不能为null
     * 如果返回null那么在后续初始化方法将报空指针异常或者通过getBean()方法获取不到bena实例对象
     * 因为后置处理器从Spring IoC容器中取出bean实例对象没有再次放回IoC容器中
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("22222B before--实例化的bean对象:"+bean+"\t"+beanName);
        // 可以根据beanName不同执行不同的处理操作
        return bean;
    }

    /**
     * 实例化、依赖注入、初始化完毕时执行
     * 注意：方法返回值不能为null
     * 如果返回null那么在后续初始化方法将报空指针异常或者通过getBean()方法获取不到bena实例对象
     * 因为后置处理器从Spring IoC容器中取出bean实例对象没有再次放回IoC容器中
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("2222222B after...实例化的bean对象:"+bean+"\t"+beanName);
        // 可以根据beanName不同执行不同的处理操作
        return bean;
    }
    //@Override
    public int getOrder() {
        // TODO Auto-generated method stub
        return 2;
    }
}
/*

----------000----------------
------UserMapperImpl，实例化--------
22222B before--实例化的bean对象:org.springframework.jdbc.datasource.DriverManagerDataSource@2173f6d9	dataSource
2222222B after...实例化的bean对象:org.springframework.jdbc.datasource.DriverManagerDataSource@2173f6d9	dataSource
22222B before--实例化的bean对象:org.mybatis.spring.SqlSessionFactoryBean@75881071	sqlSessionFactory
Logging initialized using 'class org.apache.ibatis.logging.stdout.StdOutImpl' adapter.
Class not found: org.jboss.vfs.VFS
JBoss 6 VFS API is not available in this environment.
Class not found: org.jboss.vfs.VirtualFile
VFS implementation org.apache.ibatis.io.JBoss6VFS is not valid in this environment.
Using VFS adapter org.apache.ibatis.io.DefaultVFS
Find JAR URL: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo
Not a JAR: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo
Reader entry: User.class
Listing file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo
Find JAR URL: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo/User.class
Not a JAR: file:/Users/konnysnow/workspace/b/spring_kuang/srping-10-mybatis-spring01/target/classes/com/kuang/pojo/User.class
Reader entry: ����   4 ]
Checking to see if class com.kuang.pojo.User matches criteria [is assignable to Object]
2222222B after...实例化的bean对象:org.mybatis.spring.SqlSessionFactoryBean@75881071	sqlSessionFactory
2222222B after...实例化的bean对象:org.apache.ibatis.session.defaults.DefaultSqlSessionFactory@72a7c7e0	sqlSessionFactory
22222B before--实例化的bean对象:org.mybatis.spring.SqlSessionTemplate@1cbbffcd	sqlSessionTemplate
2222222B after...实例化的bean对象:org.mybatis.spring.SqlSessionTemplate@1cbbffcd	sqlSessionTemplate
------UserMapperImpl，依赖注入setSqlSessionTemplate=[org.mybatis.spring.SqlSessionTemplate@1cbbffcd]--------
------UserMapperImpl，依赖注入setName=[oscar]--------
22222B before--实例化的bean对象:com.kuang.mapper.UserMapperImpl@27ce24aa	userMapper
------UserMapperImpl，初始化方法testProcessInit,测试后置处理器--------
2222222B after...实例化的bean对象:com.kuang.mapper.UserMapperImpl@27ce24aa	userMapper
----------111----------------
 */