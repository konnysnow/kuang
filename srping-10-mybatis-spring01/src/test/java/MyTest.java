import com.kuang.mapper.UserMapper;
import com.kuang.mapper.UserMapperImpl;
import com.kuang.pojo.User;
import com.kuang.processor.MyBeanPostProcessor;
import com.kuang.processor.MyBeanPostProcessor2;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.util.List;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        System.out.println("----------[----1111]----------------");
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println("----bean--"+beanDefinitionName);
        }
        System.out.println("----------000----------------");
        UserMapper userMapper = context.getBean("userMapper", UserMapper.class);//2个都可以用
        System.out.println("----------1----------------");
//        UserMapperImpl userMapper = context.getBean("userMapper", UserMapperImpl.class);
        List<User> users = userMapper.selectUser();
        for (User user : users) {
            System.out.println(user);
        }
        System.out.println("----------2----------------");

    }
    @Test
    public void  test2(){
        //new XmlBeanFactory()

        XmlBeanFactory bf = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
        // 显示添加后置处理器
        bf.addBeanPostProcessor(bf.getBean(MyBeanPostProcessor2.class));
        String[] beanDefinitionNames = bf.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println("----bean--"+beanDefinitionName);
        }
        System.out.println("----------000----------------");
        UserMapper userMapper = bf.getBean("userMapper", UserMapper.class);//2个都可以用
        System.out.println("----------111----------------");
        List<User> users = userMapper.selectUser();
        for (User user : users) {
            System.out.println(user);
        }
        System.out.println("----------2222----------------");
    }

/*
注意！！！

BeanFactory和ApplicationContext两个容器对待bean的后置处理器稍微有些不同。

ApplicationContext容器会自动检测Spring配置文件中那些bean所对应的Java类实现了BeanPostProcessor接口，并自动把它们注册为后置处理器。
在创建bean过程中调用它们，所以部署一个后置处理器跟普通的bean没有什么太大区别。

BeanFactory容器注册bean后置处理器时必须通过代码显示的注册，在IoC容器继承体系中的ConfigurableBeanFactory接口中定义了注册方法
 */
}
