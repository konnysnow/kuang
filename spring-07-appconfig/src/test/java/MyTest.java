import com.ks.pojo.Cat;
import com.kuang.config.KuangConfig;
import com.kuang.pojo.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MyTest {


    public static void main(String[] args) {
        ApplicationContext context =new AnnotationConfigApplicationContext(KuangConfig.class);
        Cat kiki = (Cat) context.getBean("kiki");
        System.out.println(kiki.getName());
        User getUser = (User) context.getBean("getUser");
        System.out.println(getUser.getName());


        KuangConfig kc = (KuangConfig) context.getBean(KuangConfig.class);
        System.out.println(kc);
    }

}
