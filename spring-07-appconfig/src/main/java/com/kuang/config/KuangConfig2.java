package com.kuang.config;

import com.ks.pojo.Cat;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KuangConfig2 {
    @Bean
    public Cat kiki(){
        return new Cat();
    }
}
