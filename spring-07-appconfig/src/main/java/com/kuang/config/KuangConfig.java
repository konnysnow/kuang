package com.kuang.config;

import com.ks.pojo.Cat;
import com.kuang.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(value="com.kuang")
@Import(KuangConfig2.class)
public class KuangConfig {
//https://blog.csdn.net/luojinbai/article/details/85877956

    //<bean>标签，方法名称，getUser相当于bean的id
    //方法返回值class，就相当于bean的class
    //return 返回值，就是容器返回的对象
    @Bean
    public User getUser(){
        return new User();
    }
}
