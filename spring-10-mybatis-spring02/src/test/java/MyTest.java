import com.alibaba.druid.pool.DruidDataSource;
import com.kuang.mapper.UserMapperImpl;
import com.kuang.pojo.User;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserMapperImpl bean = context.getBean(UserMapperImpl.class);
        List<User> users = bean.selectUser();
        for (User user : users) {
            System.out.println(user);
        }
        DruidDataSource dataSource = context.getBean("dataSource", DruidDataSource.class);
        System.out.println(dataSource+"]]]"+dataSource.getClass().getName());
        HikariDataSource dataSource2 = context.getBean("dataSource2", HikariDataSource.class);
        System.out.println(dataSource2);
    }

    @Test
    public void testHikariCP() throws SQLException {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("com.mysql.cj.jdbc.Driver");
        config.setJdbcUrl("jdbc:mysql://localhost:3306/all_study");
        config.setUsername("root");
        config.setPassword("19890127");

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        // HikariDataSource 也可以配置
        DataSource ds = new HikariDataSource(config);
        ((HikariDataSource) ds).setPassword("root");

        System.out.println(ds.getConnection());


        ((HikariDataSource) ds).close();
    }
}
