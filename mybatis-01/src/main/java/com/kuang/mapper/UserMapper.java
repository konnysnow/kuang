package com.kuang.mapper;

import com.kuang.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    List<User> selectUser();

    @Select("select * from t_sys_user t where t.cuid like #{cuid} or name like #{name}")
    List<User> selectByCon(@Param("cuid") String cuid, @Param("name") String name);


    List<User> selectUserByPage(Map<String,Object> map);
}
