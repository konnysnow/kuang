package com.kuang.mapper;

import com.kuang.pojo.Note;

import java.util.List;
import java.util.Map;

public interface NoteMapper {
    public int insertNote(Note note);

    public List<Note> selectNote();

    public int updateNote(Note note);
    public Note selectNoteById(Long id);
}

