package com.kuang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Note implements Serializable {
    private Long id;
    private String title;
    private String tag;
    private String content;
    private String link;
    private String remark;
    private String oriStr;
    private String createTimeStr;
    private Date createTime;
    private String updateTimeStr;
    private Date updateTime;


}
