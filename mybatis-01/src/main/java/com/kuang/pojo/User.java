package com.kuang.pojo;

import lombok.Data;

@Data
public class User {

    private String cuid;
    private String name;
}
