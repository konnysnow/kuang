import com.kuang.mapper.NoteMapper;
import com.kuang.pojo.Note;
import com.kuang.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.sql.SQLException;

public class CacheTest {

    @Test
    public void updateNote() throws SQLException {

        SqlSession sqlSession = MybatisUtils.getSqlSession();
        sqlSession.getConnection().setAutoCommit(true);
        NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);

        Note note=new Note();
        note.setId(4L);
        note.setContent("4444");
        int x = mapper.updateNote(note);
        System.out.println(x);
        sqlSession.close();
    }
}
