import com.kuang.mapper.NoteMapper;
import com.kuang.mapper.UserMapper;
import com.kuang.pojo.Note;
import com.kuang.pojo.User;
import com.kuang.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CnoteTest {

    @Test
    public void testInsertCnote(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
        Note note =new Note();
        note.setContent("1111");
        int x =mapper.insertNote(note);
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void testSelectCnote(){
        SqlSession sqlSession =null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
            List<Note> notes = mapper.selectNote();
            for (Note note : notes) {
                System.out.println(note.toString());
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(sqlSession!=null)
                sqlSession.close();
        }
    }
    @Test
    public void testCache(){
        SqlSession sqlSession =null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
            Note note = mapper.selectNoteById(4L);
            System.out.println(note.toString());

            Note note2 = mapper.selectNoteById(4L);
            System.out.println(note2.toString());

            System.out.println("1==========2???????");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(sqlSession!=null)
                sqlSession.close();
        }
    }

    @Test
    public void testLower(){
        System.out.println("ORGCODE".toLowerCase());
    }

    @Test
    public void testPg(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = mapper.selectUser();
        for (User user : users) {
            System.out.println(user);
        }
        //sqlSession.commit();

        System.out.println("~~~~~~~~~~3333~~~~~~");
        List<User> users1 = mapper.selectByCon("%10880%", "%张%");
        for (User user : users1) {
            System.out.println(user);
        }

        sqlSession.close();
    }
    @Test
    public void testPgPage(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        Map mapx = new HashMap();
        mapx.put("startIndex",3);
        mapx.put("pageSize",5);
        List<User> users = mapper.selectUserByPage(mapx);
        for (User user : users) {
            System.out.println(user);
        }
        //sqlSession.commit();
        sqlSession.close();
    }
}
