import com.kuang.mapper.NoteMapper;
import com.kuang.pojo.Note;
import com.kuang.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.sql.SQLException;

public class Cache2Test {

    @Test
    public void cache2() throws SQLException {
        Note note2=null ,note=null;
        SqlSession sqlSession =null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
             note = mapper.selectNoteById(4L);
            System.out.println(note.toString());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(sqlSession!=null)
                sqlSession.close();
        }
        System.out.println("----==========----");
        try {
            sqlSession = MybatisUtils.getSqlSession();
            NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
             note2 = mapper.selectNoteById(4L);
            System.out.println(note2.toString());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(sqlSession!=null)
                sqlSession.close();
        }
        System.out.println("----==========----"+(note==note2));//false
        System.out.println("----==========----"+(note.equals(note2)));//true
        System.out.println("----==========----"+(note.hashCode()));//hashcode相同，不代表地址相同。。。
        System.out.println("----==========----"+(note2.hashCode()));
    }


    @Test
    public void cache22() throws SQLException {
        SqlSession sqlSession =null;
        SqlSession sqlSession2 =null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
            Note note = mapper.selectNoteById(4L);
            System.out.println(note.toString());
            System.out.println("----==========----");

            sqlSession2 = MybatisUtils.getSqlSession();
            NoteMapper mapper2 = sqlSession2.getMapper(NoteMapper.class);
            Note note2 = mapper2.selectNoteById(4L);
            System.out.println(note2.toString());

            System.out.println("----==========----"+(note==note2));//false
            System.out.println("----==========----"+(note.equals(note2)));//true
            System.out.println("----==========----"+(note.hashCode()));//hashcode相同，不代表地址相同。。。
            System.out.println("----==========----"+(note2.hashCode()));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(sqlSession!=null)
                sqlSession.close();
            if(sqlSession2!=null)
                sqlSession2.close();
        }
    }


    @Test
    public void cache23() throws SQLException {
        SqlSession sqlSession =null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            NoteMapper mapper = sqlSession.getMapper(NoteMapper.class);
            Note note = mapper.selectNoteById(4L);
            System.out.println(note.toString());
            System.out.println("----==========----");

            Note note2 = mapper.selectNoteById(4L);
            System.out.println(note2.toString());

            System.out.println("----==========----"+(note==note2));//true！！！！！Cache Hit
//[com.kuang.mapper.NoteMapper]-Cache Hit Ratio [com.kuang.mapper.NoteMapper]: 0.0
            System.out.println("----==========----"+(note.equals(note2)));//true
            System.out.println("----==========----"+(note.hashCode()));//hashcode相同，不代表地址相同。。。
            System.out.println("----==========----"+(note2.hashCode()));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(sqlSession!=null)
                sqlSession.close();
        }
    }
}
