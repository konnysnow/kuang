package com.kuang.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;

public class People {
    @Resource(name="duck222")
    private Duck duck;

    @Autowired(required = false)
    private Cat cat;
    @Autowired
    @Qualifier(value = "dogppp")
    private Dog dog;

    private String name;

    //不写setter，也可以注入，但是【cat、dog】不能为null，否则启动报错
    //
//    @Autowired
//    public void setCat(Cat cat) {
//        this.cat = cat;
//    }
//    @Autowired
//    public void setDog(Dog dog) {
//        this.dog = dog;
//    }

    public Cat getCat() {
        return cat;
    }

    public Dog getDog() {
        return dog;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Duck getDuck() {
        return duck;
    }

    @Override
    public String toString() {
        return "People{" +
                "cat=" + cat +
                ", dog=" + dog +
                ", name='" + name + '\'' +
                '}';
    }
}
