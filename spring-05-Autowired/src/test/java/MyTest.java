import com.kuang.pojo.People;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
//        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        People people = context.getBean("people", People.class);
        people.getDog().shout();
        people.getCat().shout();

        people.getDuck().shout();

/*【同一个xml里，两个id=dog333】报错！！
org.springframework.beans.factory.parsing.BeanDefinitionParsingException: Configuration problem: Bean name 'dog333' is already used in this <beans> element
Offending resource: class path resource [beans.xml]
 */

/*【多个xml里，两个ID=dog333】不报错！！！！
【多个xml里，多个Dog类型的bean】报错！！Autowired是buType的！！！
        No qualifying bean of type 'com.kuang.pojo.Dog' available: expected single matching bean but found 3: dog111,dog333,dogaaa
        Exception in thread "main" org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'people':
        Unsatisfied dependency expressed through field 'dog';
        nested exception is org.springframework.beans.factory.NoUniqueBeanDefinitionException: No qualifying bean of type 'com.kuang.pojo.Dog' available: expected single matching bean but found 3: dog111,dog333,dogaaa
*/

        /**
 1。使用Autowired，默认people的bean必须全部有，否则会报错
 除非@Autowired(required = false)，可以不创建bean；
 2。people里可以不写cat的setter方法，也可以自动注入；
 3。Autowired默认ByType，所以bean可以不是cat，可以叫cat111；
 4。【同一个xml里，两个id=dog333】报错！！
         org.springframework.beans.factory.parsing.BeanDefinitionParsingException:
         Configuration problem: Bean name 'dog333' is already used in this <beans> element
         Offending resource: class path resource [beans.xml]        ；
 5。【多个xml里，两个ID=dog333】不报错！！！！
【多个xml里，多个Dog类型的bean】报错！！Autowire是buType的！！！
         Exception encountered during context initialization - cancelling refresh attempt:
         org.springframework.beans.factory.UnsatisfiedDependencyException:
         Error creating bean with name 'people': Unsatisfied dependency expressed through field 'dog';
         nested exception is org.springframework.beans.factory.NoUniqueBeanDefinitionException:
         No qualifying bean of type 'com.kuang.pojo.Dog' available:
         expected single matching bean but found 3: dog111,dog333,dogppp
因为Autowired默认是按byType来装配的。
6。【多个xml里，多个Dog类型的bean】
通过：@Qualifier(value = "dogppp") 指定bean的id，可以解决问题

7.@Resource【同一个xml里，多个bean，只要有一个id和people里的duck相同，就不会报错】
如果是Duck=【duck000】，不会报错，byType生效
如果是Duck=【duck，duck222】，不会报错，byName生效
如果是Duck=【duck111，duck222】，报错  ，都没生效
如果是Duck=【duck111，duck222】且，@Resource(name="duck222")，不会报错，byName生效
         */
    }
}
