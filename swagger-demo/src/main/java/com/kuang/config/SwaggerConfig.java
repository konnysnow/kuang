package com.kuang.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

//    @Bean
//    public Docket docket1(){
//
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("预警2")
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.kuang.controller2"))
//                .build();
//    }
    @Bean
    public Docket docket(Environment ev){
        Profiles pp = Profiles.of("dev","test");
        boolean b = ev.acceptsProfiles(pp);

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("预警")
                .enable(b)//是否开启注释
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.kuang.controller"))
//                .paths(PathSelectors.ant("/hello/**"))
                .build();
    }
    private ApiInfo apiInfo(){

        Contact konny = new Contact("konny", "", "");
        List<VendorExtension> cc= new ArrayList<VendorExtension>(){};
        return new ApiInfo("ks的swagger测试文档",
                "再小的帆也可以远航","v1.0",""
                ,konny
                ,"","",cc
                );
    }
}
