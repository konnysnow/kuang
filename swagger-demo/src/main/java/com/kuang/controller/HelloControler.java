package com.kuang.controller;

import com.kuang.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(tags = "hello控制器")
@RestController("/hello")
public class HelloControler {

    // /error
    @ApiOperation("返回hello")
    @GetMapping("/h1")
    public String h1(){
        return "hello";
    }

    //https://blog.csdn.net/qq_39930584/article/details/79960449
    @ApiOperation("返回一个用户")
    @PostMapping("/user3")
    public User h3(@ApiParam("用户名") @RequestParam String name){
        System.out.println(name);
        User u = new User();
        u.setName(name);
        return u;
    }
    @ApiOperation("返回一个用户")
    @PostMapping("/user")
    public User h2(@ApiParam("用户名") @RequestBody Map<String, Object> mapx){
        System.out.println(mapx);
        User u = new User();
        u.setName((String)mapx.get("name"));
        return u;
    }

    @ApiOperation("返回一个用户2")
    @PostMapping("/user2")
    public User h3(@ApiParam("用户名") User user){
        return user;
    }

}
