# kuang

#### 介绍
狂神说java，跟着狂神的视频代码练习，哈哈

#### 软件架构
视频地址：https://www.bilibili.com/video/BV1WE411d7Dv?p=22&spm_id_from=pageDriver
- spring-01-ioc1：【p,】
- spring-01-ioc2：【p,】
- spring-05-Autowired ：【p,】
- spring-06-anno：【p14,spring注解开发】
- spring-07-appconfig：【p15-p16,javaConfig实现配置】
- spring-08-proxy：【p17-p19,代理模式，静态代理，动态代理】
- spring-09-aop；【p20-p22,spring aop】
- spring-10-mybatis；【p23，回顾mybatis】
- spring-10-mybatis-base:【加戏，mybatis入门】
https://www.cnblogs.com/benjieqiang/p/11183580.html
https://blog.csdn.net/qq_24689877/article/details/82863431
- srping-10-mybatis-spring01：【p24,整合mybatis方式一】

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
