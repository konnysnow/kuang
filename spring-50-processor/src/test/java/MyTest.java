import com.kuang.pojo.User;
import com.kuang.processor.MyBeanFactoryPostProcessor;
import com.kuang.processor.MyBeanPostProcessor;
import com.kuang.processor.MyBeanPostProcessor2;
import org.junit.Test;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.util.logging.XMLFormatter;

public class MyTest {
    @Test
    public void test(){
        System.out.println("===============开始=");
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user = context.getBean("user", User.class);
        //user.age值的修改过程
        //1。xml,age=18,
        //2。beanFactory,age=108
        //3。init,age=208
        System.out.println("===============结束="+user.getAge());//208
    }
    @Test
    public void test2(){
        System.out.println("===============开始2=");
        XmlBeanFactory bf = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
        //2。加载beanFactory后置处理器
        MyBeanFactoryPostProcessor bfp = bf.getBean(MyBeanFactoryPostProcessor.class);
        bfp.postProcessBeanFactory(bf);

        //1。加载bean后置处理器
        bf.addBeanPostProcessor(bf.getBean(MyBeanPostProcessor2.class));
        bf.addBeanPostProcessor(bf.getBean(MyBeanPostProcessor.class));


        User user = bf.getBean(User.class);
        System.out.println("===============结束2="+user.getAge());
    }
}
/*【test()】
===============开始=
******调用BeanFactoryPostProcessor开始
bean name:cat
bean name:user
修改user的age值=108
bean name:myBeanPostProcessor
bean name:myBeanPostProcessor2
bean name:myBeanFactoryPostProcessor
******调用BeanFactoryPostProcessor结束
--后置1----before--实例化的bean对象:com.kuang.pojo.Cat@60285225	cat
--后置2----before--实例化的bean对象:com.kuang.pojo.Cat@60285225	cat
--后置1----after...实例化的bean对象:com.kuang.pojo.Cat@60285225	cat
--后置2----after...实例化的bean对象:com.kuang.pojo.Cat@60285225	cat
----user-自定义方法--
-----user-依赖注入name【druid】-----
-----user-依赖注入age【108】-----
-----user-依赖注入cat【com.kuang.pojo.Cat@60285225】-----
--后置1----before--实例化的bean对象:com.kuang.pojo.User@5f3a4b84	user
--后置2----before--实例化的bean对象:com.kuang.pojo.User@5f3a4b84	user
-----user-初始化方法-----this.age=208
--后置1----after...实例化的bean对象:com.kuang.pojo.User@5f3a4b84	user
--后置2----after...实例化的bean对象:com.kuang.pojo.User@5f3a4b84	user
===============结束=208
 */
/*【test2()】，再beanFactoryP,先beanP
===============开始2=
******调用BeanFactoryPostProcessor开始
bean name:cat
bean name:user
修改user的age值=108
bean name:myBeanPostProcessor
bean name:myBeanPostProcessor2
bean name:myBeanFactoryPostProcessor
******调用BeanFactoryPostProcessor结束
--后置2----before--实例化的bean对象:com.kuang.processor.MyBeanPostProcessor@553f17c	myBeanPostProcessor
--后置2----after...实例化的bean对象:com.kuang.processor.MyBeanPostProcessor@553f17c	myBeanPostProcessor
----user-自定义方法--
--后置2----before--实例化的bean对象:com.kuang.pojo.Cat@7d907bac	cat
--后置1----before--实例化的bean对象:com.kuang.pojo.Cat@7d907bac	cat
--后置2----after...实例化的bean对象:com.kuang.pojo.Cat@7d907bac	cat
--后置1----after...实例化的bean对象:com.kuang.pojo.Cat@7d907bac	cat
-----user-依赖注入name【druid】-----
-----user-依赖注入age【108】-----
-----user-依赖注入cat【com.kuang.pojo.Cat@7d907bac】-----
--后置2----before--实例化的bean对象:com.kuang.pojo.User@1d16f93d	user
--后置1----before--实例化的bean对象:com.kuang.pojo.User@1d16f93d	user
-----user-初始化方法-----this.age=208
--后置2----after...实例化的bean对象:com.kuang.pojo.User@1d16f93d	user
--后置1----after...实例化的bean对象:com.kuang.pojo.User@1d16f93d	user
===============结束2=208
 */
/*【test2()】,先beanP，再beanFactoryP
===============开始2=
--后置2----before--实例化的bean对象:com.kuang.processor.MyBeanPostProcessor@553f17c	myBeanPostProcessor
--后置2----after...实例化的bean对象:com.kuang.processor.MyBeanPostProcessor@553f17c	myBeanPostProcessor
--后置2----before--实例化的bean对象:com.kuang.processor.MyBeanFactoryPostProcessor@4f7d0008	myBeanFactoryPostProcessor
--后置1----before--实例化的bean对象:com.kuang.processor.MyBeanFactoryPostProcessor@4f7d0008	myBeanFactoryPostProcessor
--后置2----after...实例化的bean对象:com.kuang.processor.MyBeanFactoryPostProcessor@4f7d0008	myBeanFactoryPostProcessor
--后置1----after...实例化的bean对象:com.kuang.processor.MyBeanFactoryPostProcessor@4f7d0008	myBeanFactoryPostProcessor
******调用BeanFactoryPostProcessor开始
bean name:cat
bean name:user
修改user的age值=108
bean name:myBeanPostProcessor
bean name:myBeanPostProcessor2
bean name:myBeanFactoryPostProcessor
******调用BeanFactoryPostProcessor结束
----user-自定义方法--
--后置2----before--实例化的bean对象:com.kuang.pojo.Cat@7791a895	cat
--后置1----before--实例化的bean对象:com.kuang.pojo.Cat@7791a895	cat
--后置2----after...实例化的bean对象:com.kuang.pojo.Cat@7791a895	cat
--后置1----after...实例化的bean对象:com.kuang.pojo.Cat@7791a895	cat
-----user-依赖注入name【druid】-----
-----user-依赖注入age【108】-----
-----user-依赖注入cat【com.kuang.pojo.Cat@7791a895】-----
--后置2----before--实例化的bean对象:com.kuang.pojo.User@67b92f0a	user
--后置1----before--实例化的bean对象:com.kuang.pojo.User@67b92f0a	user
-----user-初始化方法-----this.age=208
--后置2----after...实例化的bean对象:com.kuang.pojo.User@67b92f0a	user
--后置1----after...实例化的bean对象:com.kuang.pojo.User@67b92f0a	user
===============结束2=208
 */