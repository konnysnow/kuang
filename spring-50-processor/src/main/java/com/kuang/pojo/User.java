package com.kuang.pojo;

public class User {
    private String name;
    private String age;
    private Cat cat;

    public User(){
        System.out.println("----user-自定义方法--");
    }
    public void init(){
        this.age="208";
        System.out.println("-----user-初始化方法-----this.age="+this.age);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
        System.out.println("-----user-依赖注入name【"+name+"】-----");
    }
    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
        System.out.println("-----user-依赖注入age【"+age+"】-----");
    }
    public Cat getCat() {
        return cat;
    }
    public void setCat(Cat cat) {
        this.cat = cat;
        System.out.println("-----user-依赖注入cat【"+cat+"】-----");
    }
}
